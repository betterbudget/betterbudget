import jwt from 'jwt-simple';
import config from '../config/secrets.js';

const { JWT_SECRET } = config;

export const createToken = (user) => {
  const timestamp = new Date().getTime();

  return jwt.encode({ sub: user.id, iat: timestamp }, JWT_SECRET);
};
