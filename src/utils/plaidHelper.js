import plaid from 'plaid';
import config from '../config/secrets.js';

const {
  PLAID_CLIENT_ID,
  PLAID_SECRET_SANDBOX,
  PLAID_SECRET_DEV,
  PLAID_CLIENT_NAME,
} = config;

export const getLinkTokenConfig = (id) => ({
  user: {
    client_user_id: id,
  },
  client_name: PLAID_CLIENT_NAME,
  products: ['auth', 'transactions'],
  country_codes: ['US'],
  language: 'en',
  account_filters: {
    depository: {
      account_subtypes: ['checking', 'savings'],
    },
    credit: {
      account_subtypes: ['credit card'],
    },
  },
});

export const plaidClient = new plaid.Client({
  clientID: PLAID_CLIENT_ID,
  secret: PLAID_SECRET_SANDBOX,
  env: plaid.environments.sandbox,
  options: {
    timeout: 30 * 60 * 1000,
    version: '2020-09-14',
  },
});
