import bodyParser from 'body-parser';
import cors from 'cors';
import express from 'express';
import mongoose from 'mongoose';
import morgan from 'morgan';
import passport from 'passport';
import config from './config/secrets.js';

// Database
const { MONGO_URI } = config;

mongoose.connect(MONGO_URI, {
  useNewUrlParser: true,
  useFindAndModify: false,
  useUnifiedTopology: true,
});

// Models
import('./models/index.js');

// Passport
import('./services/passport.js');

// App
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.use(morgan('combined'));

// Routes
import * as routes from './routes/index.js';
app.use('/accounts', routes.accountsRoutes);
app.use('/auth', routes.authRoutes);
app.use('/plaid', routes.plaidRoutes);
app.use('/recurring_rules', routes.recurringRulesRoutes);
app.use('/transactions', routes.transactionsRoutes);

// Server
const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
  console.log('Listening at PORT: ', PORT);
});
