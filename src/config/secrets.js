import dotenv from 'dotenv';

dotenv.config();

export default {
  MONGO_URI: process.env.MONGO_URI,
  JWT_SECRET: process.env.JWT_SECRET,
  PLAID_CLIENT_ID: process.env.PLAID_CLIENT_ID,
  PLAID_CLIENT_NAME: process.env.PLAID_CLIENT_NAME,
  PLAID_SECRET_SANDBOX: process.env.PLAID_SECRET_SANDBOX,
  PLAID_SECRET_DEV: process.env.PLAID_SECRET_DEV,
};
