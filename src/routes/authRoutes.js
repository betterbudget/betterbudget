import express from 'express';
import passport from 'passport';
import * as authController from '../controllers/auth/index.js';

const router = express.Router();

router.get('/user', authController.getUser);

router.post('/signup', authController.postSignUp);

router.post(
  '/signin',
  passport.authenticate('local', { session: false }),
  authController.postSignIn,
);

router.get(
  '/signOut',
  passport.authenticate('jwt', { session: false }),
  authController.postSignOut,
);

export default router;
