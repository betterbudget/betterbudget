import express from 'express';
import passport from 'passport';
import * as transactionsController from '../controllers/transactions/index.js';

const router = express.Router();

router.post('/', transactionsController.createTransaction);

router.get(
  '/',
  passport.authenticate('jwt', { session: false }),
  transactionsController.fetchAllTransactions,
);

router.get('/:id', transactionsController.fetchTransaction);

router.put('/:id', transactionsController.updateTransaction);

router.put('/link/:id', transactionsController.linkTransactions);

router.delete('/:id', transactionsController.deleteTransaction);

export default router;
