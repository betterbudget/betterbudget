import express from 'express';
import * as recurringRulesController from '../controllers/recurringRules/index.js';

const router = express.Router();

router.post('/', recurringRulesController.createRecurringRule);

router.get('/', recurringRulesController.fetchAllRecurringRules);

router.get('/:id', recurringRulesController.fetchRecurringRule);

router.put('/:id', recurringRulesController.updateRecurringRule);

router.delete('/:id', recurringRulesController.deleteRecurringRule);

export default router;
