import express from 'express';
import passport from 'passport';
import * as plaidController from '../controllers/plaid/index.js';

const router = express.Router();

router.get(
  '/link_token',
  passport.authenticate('jwt', { session: false }),
  plaidController.createLinkToken,
);

router.post(
  '/connect_bank',
  passport.authenticate('jwt', { session: false }),
  plaidController.connectBank,
);

export default router;
