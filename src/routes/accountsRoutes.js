import express from 'express';
import * as accountsController from '../controllers/accounts/index.js';

const router = express.Router();

router.post('/', accountsController.createAccount);

router.get('/', accountsController.fetchAllAccounts);

router.get('/:id', accountsController.fetchAccount);

router.put('/:id', accountsController.updateAccount);

router.delete('/:id', accountsController.deleteAccount);

export default router;
