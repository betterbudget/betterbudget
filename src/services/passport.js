import passport from 'passport';
import passportJwt from 'passport-jwt';
import passportLocal from 'passport-local';
import config from '../config/secrets.js';
import User from '../models/User.js';

const { Strategy: JwtStrategy, ExtractJwt } = passportJwt;
const { Strategy: LocalStrategy } = passportLocal;

const SECRET = config.JWT_SECRET;

const localOptions = { usernameField: 'email' };
const localLogin = new LocalStrategy(
  localOptions,
  (email, password, done) => {
    try {
      User.findOne({ email }, (err, user) => {
        if (err) return done(err);
        if (!user) return done(null, false);

        // if user exists, compare password
        user.comparePassword(password, function (err, isMatch) {
          if (err) return done(err);
          if (!isMatch) return done(null, false);

          return done(null, user);
        });
      });
    } catch (err) {
      done(err);
    }
  },
);

const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromHeader('authorization'),
  secretOrKey: SECRET,
};
const jwtLogin = new JwtStrategy(jwtOptions, (payload, done) => {
  try {
    User.findById(payload.sub, (err, user) => {
      if (err) done(err, false);

      if (user) done(null, user);
      else done(null, false);
    });
  } catch (err) {
    done(err);
  }
});

// export default function (passport) {
passport.use(localLogin);
passport.use(jwtLogin);

// return passport;
// }
