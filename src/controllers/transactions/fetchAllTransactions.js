import moment from 'moment';
// import Account from '../../models/Account.js';
import Item from '../../models/Item.js';
import Transaction from '../../models/Transaction.js';
// import User from '../../models/User.js';
import { plaidClient } from '../../utils/plaidHelper.js';

export default async function fetchAllTransactions(req, res) {
  const { user } = req;

  if (!user) return res.status(400).send('User not found');

  const { accounts, memberSince } = user;

  const transactionsStart = moment
    .utc(memberSince)
    .subtract(1, 'year')
    .format('YYYY-MM-DD');
  const transactionsEnd = moment.utc().format('YYYY-MM-DD');

  const allTransactions = [];

  for (const account of accounts) {
    const {
      account_id: accountId,
      account_name: accountName,
      institution_name: institutionName,
    } = account;
    const itemId = account._item;

    let accessToken;

    try {
      const item = await Item.findById(itemId);

      accessToken = item.access_token;
    } catch (err) {
      const message = `No Item found for item _id ${itemId}`;

      throw new Error(message);
    }

    if (!accountId || !accessToken) return transactionsAcc;

    const plaidTransactionsResponse =
      await plaidClient.getTransactions(
        accessToken,
        transactionsStart,
        transactionsEnd,
        {
          account_ids: [accountId],
        },
      );

    const { transactions: transactionsData } =
      plaidTransactionsResponse;

    for (const transaction of transactionsData) {
      const {
        transaction_id,
        category,
        category_id,
        transaction_type,
        name: transactionName,
        amount,
        iso_currency_code: currency_code,
        date,
        pending,
        account_id,
      } = transaction;

      allTransactions.push(
        new Transaction({
          transaction_id,
          institution_name: institutionName,
          category: JSON.stringify(category),
          category_id,
          transaction_type,
          name: transactionName,
          amount,
          currency_code,
          date,
          pending,
          account_id,
          account_name: accountName,
        }),
      );
    }
  }

  res.send(allTransactions);
}
