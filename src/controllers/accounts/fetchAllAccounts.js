import Account from '../../models/Account.js';

export default async function fetchAllAccounts(req, res) {
  const { user } = req;

  if (!user) return res.status(400).send('User not found');

  const accounts = await Account.find({ _user: id });

  return accounts;
}
