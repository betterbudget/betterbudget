import Account from '../../models/Account.js';

export default async function fetchAccount(req, res) {
  const id = req.params?.id;

  const account = await Account.findOne({ account_id: id });

  res.send(account);
}
