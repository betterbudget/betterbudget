import User from '../../models/User.js';
import { createToken } from '../../utils/helpers.js';

export default async function postSignUp(req, res, next) {
  const { firstName, lastName, email, password } = req.body;

  User.findOne({ email }, (err, existingUser) => {
    console.log(err);
    if (err) return next(err);
    if (existingUser)
      return res.status(422).send('Email is already in use');

    const user = new User({
      firstName,
      lastName,
      email,
      password,
    });

    user.save(function (err) {
      if (err) return next(err);

      const { _id, memberSince } = user;
      const token = createToken(user);

      res.send({
        user: {
          _id,
          accounts: [],
          email,
          firstName,
          lastName,
          memberSince,
        },
        token,
      });
    });
  });
}
