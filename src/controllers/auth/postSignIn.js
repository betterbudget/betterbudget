import { createToken } from '../../utils/helpers.js';

export default async function postSignIn(req, res) {
  const { _id, email, firstName, lastName, memberSince, accounts } =
    req.user;
  const token = createToken(req.user);

  res.send({
    user: {
      _id,
      email,
      firstName,
      lastName,
      memberSince,
      accounts,
    },
    token,
  });
}
