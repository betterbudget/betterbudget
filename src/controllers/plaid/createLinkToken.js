import {
  getLinkTokenConfig,
  plaidClient,
} from '../../utils/plaidHelper.js';

export default async function createLinkToken(req, res) {
  const _id = req.user?._id;

  const linkTokenConfig = getLinkTokenConfig(_id);

  try {
    const linkTokenResponse = await plaidClient.createLinkToken(
      linkTokenConfig,
    );

    res.send(linkTokenResponse);
  } catch (err) {
    return res.status(400).send(err);
  }
}
