import { plaidClient } from '../../utils/plaidHelper.js';
import Account from '../../models/Account.js';
import Item from '../../models/Item.js';

export default async function (req, res) {
  const { user = {}, body = {} } = req || {};
  const { _id: userId, accounts: currentUserAccounts } = user;

  const { public_token, metadata = {} } = body;

  try {
    const {
      accounts: accountsMetadata,
      institution: { name: institutionName, institution_id } = {},
    } = metadata;

    const newAccountIds = accountsMetadata.reduce(
      (idsAcc, accountMetadata) => {
        const {
          id: accountId,
          name: accountName,
          mask,
        } = accountMetadata;

        const existingAccount = currentUserAccounts.find(
          (account) => {
            return (
              account.account_id === accountId &&
              account.account_name === accountName &&
              account.mask === mask
            );
          },
        );

        return [...idsAcc, ...(!existingAccount ? [accountId] : [])];
      },
      [],
    );

    const savedAccounts = [];

    if (newAccountIds.length) {
      const exchangePublicTokenRes =
        await plaidClient.exchangePublicToken(public_token);
      const { access_token } = exchangePublicTokenRes;

      const plaidAccountsRes = await plaidClient.getAccounts(
        access_token,
        {
          account_ids: newAccountIds,
        },
      );

      const { accounts: plaidAccounts, item: { item_id } = {} } =
        plaidAccountsRes || {};

      const newItem = new Item({
        item_id,
        access_token,
        institution_id,
        institution_name: institutionName,
      });

      const savedItem = await newItem.save();

      for (const plaidAccount of plaidAccounts) {
        const {
          account_id,
          mask,
          name: accountName,
          official_name,
          type,
          subtype,
          balances: {
            current: balance,
            iso_currency_code: currency_code,
          },
        } = plaidAccount;

        const newAccount = new Account({
          account_id,
          institution_name: institutionName,
          mask,
          account_name: accountName,
          display_name: accountName,
          official_name,
          type,
          subtype,
          balance,
          currency_code,
          _user: userId,
          _item: savedItem._id,
        });

        const savedAccount = await newAccount.save();

        user.accounts.push(savedAccount);

        savedAccounts.push(savedAccount);
      }

      await user.save();
    }

    res.send({
      accounts: savedAccounts,
    });
  } catch (err) {
    return res.status(400).send(err.message);
  }
}
