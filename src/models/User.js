import bcrypt from 'bcrypt';
import moment from 'moment';
import mongoose from 'mongoose';
import { AccountSchema } from './Account.js';

const Schema = mongoose.Schema;

const SALT_ROUNDS = 10;

export const UserSchema = new Schema({
  firstName: String,
  lastName: String,
  email: String,
  password: String,
  lastLogin: { type: Date, default: moment.utc().format() },
  memberSince: { type: Date, default: moment.utc().format() },
  accounts: [AccountSchema],
});

UserSchema.pre('save', function (next) {
  const user = this;
  if (!user.isModified('password')) return next();

  bcrypt.genSalt(SALT_ROUNDS, function (err, salt) {
    if (err) throw err;

    bcrypt.hash(user.password, salt, function (err, hash) {
      if (err) return next(err);

      user.password = hash;
      next();
    });
  });
});

UserSchema.methods.comparePassword = function (
  candidatePassword,
  callback,
) {
  bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
    if (err) return callback(err);

    callback(null, isMatch);
  });
};

export default mongoose.model('User', UserSchema);
