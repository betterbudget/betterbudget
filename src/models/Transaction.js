import mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const TransactionSchema = new Schema({
  transaction_id: String,
  institution_name: String,
  category: String,
  category_id: String,
  transaction_type: String,
  account_name: String,
  name: String,
  amount: Number,
  currency_code: String,
  date: Date,
  pending: Boolean,
  note: String,
  lastUpdated: Date,
  _account: {
    type: Schema.Types.ObjectId,
    ref: 'Account',
  },
  _recurringRule: {
    type: Schema.Types.ObjectId,
    ref: 'RecurringRule',
  },
});

TransactionSchema.virtual('isFuture').get(function () {
  return this.pending && this._recurring;
});

TransactionSchema.virtual('isSettled').get(function () {
  return !this.pending && this._recurring;
});

export default mongoose.model('Transaction', TransactionSchema);
