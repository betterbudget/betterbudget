import mongoose from 'mongoose';
const Schema = mongoose.Schema;

export const ItemSchema = new Schema({
  item_id: String,
  access_token: String,
  institution_id: String,
  institution_name: String,
});

export default mongoose.model('Item', ItemSchema);
