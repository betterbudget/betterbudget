import mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const RecurringRuleSchema = new Schema({
  description: String,
  category: String,
  amount: Number,
  frequency: String,
  weekNumber: [Number],
  weekDay: [String],
  monthDay: [Number],
  ruleString: String,
  ruleText: String,
  startDate: Date,
  endDate: Date,
  _account: {
    type: Schema.Types.ObjectId,
    ref: 'Account',
  },
});

export default mongoose.model('RecurringRule', RecurringRuleSchema);
