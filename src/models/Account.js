import moment from 'moment';
import mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const AccountSchema = new mongoose.Schema({
  account_id: String,
  institution_name: String,
  mask: String,
  account_name: String,
  official_name: String,
  display_name: String,
  type: String,
  subtype: String,
  balance: Number,
  currency_code: String,
  lastUpdated: { type: Date, default: moment.utc().format() },
  _user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
  _item: {
    type: Schema.Types.ObjectId,
    ref: 'Item',
  },
});

export default mongoose.model('Account', AccountSchema);
