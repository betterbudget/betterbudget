import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import Dashboard from './features/Dashboard';
import Nav from './features/Nav';
import SignIn from './features/auth/SignIn';
import SignOut from './features/auth/SignOut';
import SignUp from './features/auth/SignUp';

export default function App() {
  return (
    <div>
      <BrowserRouter>
        <header>
          <Nav />
          <h1>Better Budget</h1>
        </header>
        <main>
          <Route exact path="/dashboard" component={Dashboard} />
          <Route exact path="/signup" component={SignUp} />
          <Route exact path="/signin" component={SignIn} />
          <Route exact path="/signout" component={SignOut} />
        </main>
      </BrowserRouter>
    </div>
  );
}
