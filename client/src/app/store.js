import { configureStore } from '@reduxjs/toolkit';
import accountsReducer from '../features/accounts/accountsSlice';
import authReducer from '../features/auth/authSlice';
import plaidReducer from '../features/plaid/plaidSlice';
import recurringRulesReducer from '../features/recurringRules/recurringRulesSlice';
import statusReducer from '../features/status/statusSlice';
import transactionsReducer from '../features/transactions/transactionsSlice';

export default configureStore({
  reducer: {
    accounts: accountsReducer,
    auth: authReducer,
    plaid: plaidReducer,
    recurringRules: recurringRulesReducer,
    status: statusReducer,
    transactions: transactionsReducer,
  },
});
