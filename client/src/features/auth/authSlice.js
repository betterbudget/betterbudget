import axios from 'axios';
import { createSlice } from '@reduxjs/toolkit';
import {
  actionLoading,
  actionSucceeded,
  actionFailed,
} from '../status/statusSlice';
import { createLinkToken } from '../plaid/plaidSlice';
import { accountFetched } from '../accounts/accountsSlice';
import { fetchAllTransactions } from '../transactions/transactionsSlice';
import { getToken, setToken, removeToken } from '../../utils/helpers';

export const authSlice = createSlice({
  name: 'auth',
  initialState: {
    token: getToken(),
    user: {},
  },
  reducers: {
    signedIn: (state, action) => {
      console.log('signed in');
      const { token, user } = action.payload;

      state.token = token;
      state.user = user;
    },
    signedUp: (state, action) => {
      console.log('signed up');
      const { token, user } = action.payload;

      state.token = token;
      state.user = user;
    },
    signedOut: (state, action) => {
      console.log('signed out');
      state.token = '';
      state.user = {};
    },
  },
  extraReducers: {},
});

// actions
export const { signedIn, signedUp, signedOut } = authSlice.actions;

// thunks
export const signIn =
  ({ values }, callback) =>
  async (dispatch) => {
    dispatch(actionLoading());

    try {
      const signInResponse = await axios.post('/auth/signin', values);

      const { data: { token, user } = {} } = signInResponse || {};

      if (user) {
        const { accounts } = user;
        setToken(token);

        accounts.forEach((account) => {
          dispatch(accountFetched(account));
        });

        dispatch(signedIn({ token, user }));
        callback();

        await Promise.all([
          dispatch(createLinkToken()),
          dispatch(fetchAllTransactions()),
        ]);
        
        return dispatch(actionSucceeded());
      }
    } catch (err) {
      dispatch(actionFailed('Unauthorized'));
    }
  };

export const signUp =
  ({ values }, callback) =>
  async (dispatch) => {
    dispatch(actionLoading());

    try {
      const res = await axios.post('/auth/signup', values);

      const { data = {} } = res || {};
      const { token } = data;

      setToken(token);

      await dispatch(createLinkToken());

      dispatch(signedUp(data));
      dispatch(actionSucceeded());

      callback();
    } catch (err) {
      const message = err?.response?.data;

      dispatch(actionFailed(message));
    }
  };

export const signOut = (callback) => async (dispatch) => {
  removeToken();

  dispatch(signedOut());
  dispatch(actionSucceeded());

  callback();
};

// selectors
export const selectToken = (state) => state.auth.token;
export const selectUser = (state) => state.auth.user;
export const selectFirstName = (state) => state.auth.user.firstName;
export const selectEmail = (state) => state.auth.user.email;

export default authSlice.reducer;
