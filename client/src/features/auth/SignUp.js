import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { signUp, selectToken, selectUser } from './authSlice';

export default function SignUp() {
  const dispatch = useDispatch();
  const history = useHistory();

  const token = useSelector(selectToken);
  const user = useSelector(selectUser);

  const { handleSubmit, register, errors } = useForm();

  const onSubmit = (values) => {
    dispatch(
      signUp({ values }, () => {
        history.push('/dashboard');
      }),
    );
  };

  useEffect(() => {
    if (token && user.email) history.push('/dashboard');
  });

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <input
        name="firstName"
        placeholder="First Name"
        defaultValue="Bryce"
        type="text"
        ref={register({
          required: 'Required',
        })}
      />
      {errors.firstName && errors.firstName.message}
      <input
        name="lastName"
        placeholder="Last Name"
        defaultValue="Richards"
        type="text"
        ref={register({
          required: 'Required',
        })}
      />
      {errors.lastName && errors.lastName.message}
      <input
        name="email"
        placeholder="Email"
        type="email"
        ref={register({
          required: 'Required',
          pattern: {
            value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
            message: 'invalid email address',
          },
        })}
      />
      {errors.email && errors.email.message}

      <input
        name="password"
        placeholder="Password"
        type="password"
        ref={register({
          required: 'Required',
        })}
      />
      {errors.password && errors.password.message}

      <button type="submit" className="button">
        Submit
      </button>
    </form>
  );
}
