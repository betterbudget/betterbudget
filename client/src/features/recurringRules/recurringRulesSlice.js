import axios from 'axios';
import { createSlice } from '@reduxjs/toolkit';
import {
  actionLoading,
  actionSucceeded,
  actionFailed,
} from '../status/statusSlice';
import { getToken } from '../../utils/helpers';

export const recurringRulesSlice = createSlice({
  name: 'recurringRules',
  initialState: {},
  reducers: {
    recurringRuleCreated: (state, action) => {
      const { _id } = action.payload;
      state[_id] = action.payload;
    },
    allRecurringRulesFetched: (state, action) => {
      return action.payload;
    },
    recurringRuleFetched: (state, action) => {
      const { _id } = action.payload;
      state[_id] = action.payload;
    },
    recurringRuleUpdated: (state, action) => {
      const { _id } = action.payload;
      state[_id] = action.payload;
    },
    recurringRuleDeleted: (state, action) => {
      const { _id } = action.payload;
      delete state[_id];
    },
  },
  extraReducers: {},
});

// actions
export const {
  recurringRuleCreated,
  allRecurringRulesFetched,
  recurringRuleFetched,
  recurringRuleUpdated,
  recurringRuleDeleted,
} = recurringRulesSlice.actions;

// thunks
export const createRecurringRule = (recurring) => async (
  dispatch,
) => {
  dispatch(actionLoading());

  try {
    const res = await axios.post('/recurring_rule', recurring, {
      headers: { authorization: getToken() },
    });

    dispatch(recurringRuleCreated(res.data));
    dispatch(actionSucceeded());
  } catch (err) {
    dispatch(actionFailed(err));
  }
};

export const fetchAllRecurringRuless = () => async (dispatch) => {
  dispatch(actionLoading());

  try {
    const res = await axios.get('/recurring_rules', {
      headers: { authorization: getToken() },
    });

    dispatch(allRecurringRulesFetched(res.data));
    dispatch(actionSucceeded());
  } catch (err) {
    dispatch(actionFailed(err));
  }
};

export const fetchRecurringRule = (_id) => async (dispatch) => {
  dispatch(actionLoading());

  try {
    const res = await axios.get(`/recurring_rules/${_id}`, {
      headers: { authorization: getToken() },
    });

    dispatch(recurringRuleFetched(res.data));
    dispatch(actionSucceeded());
  } catch (err) {
    dispatch(actionFailed(err));
  }
};

export const updateRecurringRule = (recurring) => async (
  dispatch,
) => {
  const { _id } = recurring;
  dispatch(actionLoading());

  try {
    const res = await axios.put(
      `/recurring_rules/${_id}`,
      recurring,
      {
        headers: { authorization: getToken() },
      },
    );

    dispatch(recurringRuleUpdated(res.data));
    dispatch(actionSucceeded());
  } catch (err) {
    dispatch(actionFailed(err));
  }
};

export const deleteRecurringRule = (_id) => async (dispatch) => {
  dispatch(actionLoading());

  try {
    const res = await axios.delete(`/recurring_rules/${_id}`, {
      headers: { authorization: getToken() },
    });

    dispatch(recurringRuleDeleted(res.data));
    dispatch(actionSucceeded());
  } catch (err) {
    dispatch(actionFailed(err));
  }
};

export default recurringRulesSlice.reducer;
