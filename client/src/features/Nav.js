import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { selectToken, selectUser } from './auth/authSlice';
import PlaidAuth from './plaid/PlaidAuth';
import Status from './status/Status';

export default function Header() {
  const token = useSelector(selectToken);
  const user = useSelector(selectUser);

  const renderLinks = () => {
    if (token && user.email) {
      return (
        <ul className="menu">
          <li>
            <Link to="/signout">Sign Out</Link>
          </li>
          <PlaidAuth />
        </ul>
      );
    } else {
      return (
        <ul className="menu">
          <li>
            <Link to="/signup">Sign Up</Link>
          </li>
          <li>
            <Link to="/signin">Sign In</Link>
          </li>
        </ul>
      );
    }
  };

  return (
    <div className="top-bar">
      <div className="top-bar-left">
        <ul className="menu">
          <li className="menu-text">
            <Status />
          </li>
        </ul>
      </div>
      <div className="top-bar-right">{renderLinks()}</div>
    </div>
  );
}
