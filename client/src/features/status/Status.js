import React from 'react';
import { useSelector } from 'react-redux';
import { selectStatus, selectError } from './statusSlice';

export default function Status() {
  const error = useSelector(selectError);
  const status = useSelector(selectStatus);

  return (
    <div>
      <div>{status}</div>
      {error && <div>{error}</div>}
    </div>
  );
}
