import { createSlice } from '@reduxjs/toolkit';
import { stateStatus } from '../../utils/constants';

export const statusSlice = createSlice({
  name: 'status',
  initialState: {
    status: stateStatus.IDLE,
    error: null,
  },
  reducers: {
    actionLoading: (state, action) => {
      state.status = stateStatus.LOADING;
      state.error = null;
    },
    actionSucceeded: (state, action) => {
      state.status = stateStatus.SUCCEEDED;
      state.error = null;
    },
    actionFailed: (state, action) => {
      state.status = stateStatus.FAILED;
      state.error = action.payload;
    },
  },
});

export const {
  actionLoading,
  actionSucceeded,
  actionFailed,
} = statusSlice.actions;

export const selectStatus = (state) => state.status.status;
export const selectError = (state) => state.status.error;

export default statusSlice.reducer;
