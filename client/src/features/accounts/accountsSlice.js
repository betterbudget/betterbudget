import axios from 'axios';
import { createSlice } from '@reduxjs/toolkit';
import { createSelector } from 'reselect';
import {
  actionLoading,
  actionSucceeded,
  actionFailed,
} from '../status/statusSlice';
import { getToken } from '../../utils/helpers';

export const accountsSlice = createSlice({
  name: 'accounts',
  initialState: {},
  reducers: {
    accountCreated: (state, action) => {
      const { _id } = action.payload;
      state[_id] = action.payload;
    },
    allAccountsFetched: (state, action) => {
      return action.payload;
    },
    accountFetched: (state, action) => {
      const { _id } = action.payload;
      state[_id] = action.payload;
    },
    accountUpdated: (state, action) => {
      const { _id } = action.payload;
      state[_id] = action.payload;
    },
    accountDeleted: (state, action) => {
      const { _id } = action.payload;
      delete state[_id];
    },
  },
  extraReducers: {},
});

// actions
export const {
  accountCreated,
  allAccountsFetched,
  accountFetched,
  accountUpdated,
  accountDeleted,
} = accountsSlice.actions;

// thunks
export const createAccount = (account) => async (dispatch) => {
  dispatch(actionLoading());

  try {
    const res = await axios.post('/accounts', account, {
      headers: { authorization: getToken() },
    });

    dispatch(accountCreated(res.data));
    dispatch(actionSucceeded());
  } catch (err) {
    dispatch(actionFailed(err));
  }
};

export const fetchAllAccounts = () => async (dispatch) => {
  dispatch(actionLoading());

  try {
    const fetchAllAccountsResponse = await axios.get('/accounts', {
      headers: { authorization: getToken() },
    });

    const accounts = fetchAllAccountsResponse.data;

    accounts.forEach((account) => {
      dispatch(accountFetched(account));
    });

    dispatch(actionSucceeded());
  } catch (err) {
    dispatch(actionFailed(err?.message));
  }
};

export const fetchAccount = (id) => async (dispatch) => {
  dispatch(actionLoading());

  try {
    const res = await axios.get(`/accounts/${id}`);

    dispatch(accountFetched(res.data));
    dispatch(actionSucceeded());
  } catch (err) {
    dispatch(actionFailed(err));
  }
};

export const updateAccount = (account) => async (dispatch) => {
  dispatch(actionLoading());

  try {
    const id = account?._id;

    const res = await axios.put(`/accounts/${id}`, account, {
      headers: { authorization: getToken() },
    });

    dispatch(accountUpdated(res.data));
    dispatch(actionSucceeded());
  } catch (err) {
    dispatch(actionFailed(err));
  }
};

export const deleteAccount = (id) => async (dispatch) => {
  dispatch(actionLoading());

  try {
    const res = await axios.delete(`/accounts/${id}`, {
      headers: { authorization: getToken() },
    });

    dispatch(accountDeleted(res.data));
    dispatch(actionSucceeded());
  } catch (err) {
    dispatch(actionFailed(err));
  }
};

// selectors
export const selectAccounts = (state) => state.accounts;
export const selectDepositoryAccounts = (state) =>
  state.accounts.filter((account) => account.type === 'depository');
export const selectCreditAccounts = (state) =>
  state.accounts.filter((account) => account.type === 'credit');
export const selectTotalCash = (state) => {};
export const selectTotalDebt = (state) => {};

export default accountsSlice.reducer;
