import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { selectToken, selectUser } from './auth/authSlice';
import requireAuth from './auth/requireAuth';
import TransactionsContainer from './transactions/TransactionsContainer';

function Dashboard() {
  const history = useHistory();
  const token = useSelector(selectToken);
  const user = useSelector(selectUser);

  useEffect(() => {
    if (!token || !user.email) history.push('/');
  });

  return (
    <div>
      {user.firstName && <div>{user.firstName}</div>}
      <TransactionsContainer />
    </div>
  );
}

export default requireAuth(Dashboard);
