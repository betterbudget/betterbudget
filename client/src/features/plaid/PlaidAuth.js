import React, { useEffect, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { usePlaidLink } from 'react-plaid-link';
import {
  createLinkToken,
  connectBank,
  selectLinkToken,
  selectPublicToken,
} from '../plaid/plaidSlice';
import { actionFailed } from '../status/statusSlice';

export default function PlaidAuth() {
  const dispatch = useDispatch();
  const linkToken = useSelector(selectLinkToken);
  const storedPublicToken = useSelector(selectPublicToken);

  useEffect(() => {
    if (!linkToken) dispatch(createLinkToken());
  });

  const onSuccess = useCallback(
    (public_token, metadata) => {
      dispatch(connectBank({ public_token, metadata }));
    },
    [dispatch],
  );

  const onExit = useCallback(
    (err, metadata) => {
      dispatch(actionFailed(err));

      if (err?.error_code === 'INVALID_LINK_TOKEN') {
        createLinkToken();
      }
    },
    [dispatch],
  );

  const config = {
    token: linkToken,
    onSuccess,
    onExit,
  };

  const { open, ready, error } = usePlaidLink(config);

  return (
    <div>
      <button
        type="submit"
        className="button"
        onClick={() => open()}
        disabled={!ready || !linkToken}
      >
        Connect a bank account
      </button>
      Link Token: {linkToken}
      Public Token: {storedPublicToken}
    </div>
  );
}
