import axios from 'axios';
import { createSlice } from '@reduxjs/toolkit';
import {
  actionLoading,
  actionSucceeded,
  actionFailed,
} from '../status/statusSlice';
import { accountCreated } from '../accounts/accountsSlice';
import { fetchAllTransactions } from '../transactions/transactionsSlice';
import { getToken } from '../../utils/helpers';

export const plaidSlice = createSlice({
  name: 'plaid',
  initialState: {
    linkToken: '',
    publicToken: '',
  },
  reducers: {
    linkTokenCreated: (state, action) => {
      state.linkToken = action.payload;

      return state;
    },
    publicTokenCreated: (state, action) => {
      state.publicToken = action.payload;

      return state;
    },
  },
  extraReducers: {},
});

// actions
export const { linkTokenCreated, publicTokenCreated } =
  plaidSlice.actions;

// thunks
export const createLinkToken = (id) => async (dispatch) => {
  dispatch(actionLoading());

  try {
    const res = await axios.get('/plaid/link_token', {
      headers: { authorization: getToken() },
    });

    const { link_token: linkToken } = res?.data || {};

    if (linkToken) dispatch(linkTokenCreated(linkToken));

    return dispatch(actionSucceeded());
  } catch (err) {
    return dispatch(actionFailed(JSON.stringify(err)));
  }
};

export const connectBank =
  ({ public_token, metadata, user }) =>
  async (dispatch) => {
    dispatch(actionLoading());
    dispatch(publicTokenCreated(public_token));

    try {
      const connectBankResponse = await axios.post(
        '/plaid/connect_bank',
        {
          public_token,
          metadata,
        },
        {
          headers: { authorization: getToken() },
        },
      );

      const {
        accounts: newAccounts = [],
        // transactions: newTransactions,
      } = connectBankResponse.data || {};

      newAccounts.forEach((account) => {
        dispatch(accountCreated(account));
      });

      await dispatch(fetchAllTransactions());

      return dispatch(actionSucceeded());
    } catch (err) {
      return dispatch(actionFailed(JSON.stringify(err)));
    }
  };

// selectors
export const selectLinkToken = (state) => state.plaid.linkToken;
export const selectPublicToken = (state) => state.plaid.publicToken;

export default plaidSlice.reducer;
