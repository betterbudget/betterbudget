import React from 'react';
import { useSelector } from 'react-redux';
import { selectTransactions } from './transactionsSlice';
import TransactionsTable from './TransactionsTable';

export default function TransactionsContainer() {
  const transactions = useSelector(selectTransactions);

  return (
    <div>{transactions.length > 0 && <TransactionsTable />}</div>
  );
}
