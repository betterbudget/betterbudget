import React from 'react';
import moment from 'moment';

export const defaultTransactionsColumns = {
  account_id: '',
  institution_name: 'Institution',
  account_name: 'Account',
  _id: '',
  category: 'Category',
  name: 'Description',
  amount: 'Amount',
  date: 'Date',
  pending: 'Pending',
  balance: 'Balance',
  note: 'Note',
};

export const formatData = (column, value) => {
  switch (column) {
    case 'date':
      return value ? moment(value).format('M/D/YYYY') : null;
    case 'amount':
    case 'balance':
      return value ? `$ ${value.toFixed(2)}` : null;
    case 'category':
      try {
        const parsedValue = JSON.parse(value);
        return parsedValue.join(' - ');
      } catch {
        return value;
      }
    case 'pending':
      return value ? 'X' : null;
    default:
      return value;
  }
};

export const formatTransactionsColumns = (
  columns = defaultTransactionsColumns,
) => {
  const formattedColumns = [];

  for (const [key, value] of Object.entries(columns)) {
    formattedColumns.push({
      Header: value,
      accessor: key,
      Cell: (props) => (
        <React.Fragment>
          {formatData(key, props.value)}
        </React.Fragment>
      ),
    });
  }

  return formattedColumns;
};

export const formatTransactionsData = (data) => {};
