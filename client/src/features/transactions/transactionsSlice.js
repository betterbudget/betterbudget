import axios from 'axios';
import { createSlice } from '@reduxjs/toolkit';
import {
  actionLoading,
  actionSucceeded,
  actionFailed,
} from '../status/statusSlice';
import { getToken } from '../../utils/helpers';

export const transactionsSlice = createSlice({
  name: 'transactions',
  initialState: [],
  reducers: {
    transactionsCreated: (state, action) => {
      const transactions = Array.isArray(action.payload)
        ? action.payload
        : [action.payload];

      return [...state, ...transactions];
    },
    allTransactionsFetched: (state, action) => {
      return action.payload;
    },
    transactionFetched: (state, action) => {
      state.push(action.payload);

      return state;
    },
    transactionUpdated: (state, action) => {
      const { _id: id } = action.payload;

      state.map((transaction) => {
        if (transaction._id === id) {
          return action.payload;
        }

        return transaction;
      });

      return state;
    },
    transactionDeleted: (state, action) => {
      const { _id: id } = action.payload;

      delete state[id];

      return state;
    },
  },
  extraReducers: {},
});

// actions
export const {
  transactionsCreated,
  allTransactionsFetched,
  transactionFetched,
  transactionUpdated,
  transactionDeleted,
} = transactionsSlice.actions;

// thunks
export const createTransaction =
  (transaction) => async (dispatch) => {
    dispatch(actionLoading());

    try {
      const res = await axios.post('/transactions', transaction, {
        headers: { authorization: getToken() },
      });

      const { data } = res || {};

      dispatch(transactionsCreated(data));

      return dispatch(actionSucceeded());
    } catch (err) {
      console.log('error :', err);

      return dispatch(actionFailed(err));
    }
  };

export const fetchAllTransactions = () => async (dispatch) => {
  dispatch(actionLoading());

  try {
    const fetchAllTransactionsResponse = await axios.get(
      '/transactions',
      {
        headers: { authorization: getToken() },
      },
    );

    const { data } = fetchAllTransactionsResponse || {};

    dispatch(allTransactionsFetched(data));

    return dispatch(actionSucceeded());
  } catch (err) {
    console.log('error :', err);

    return dispatch(actionFailed(err.message));
  }
};

export const fetchTransaction = (_id) => async (dispatch) => {
  dispatch(actionLoading());

  try {
    const res = await axios.get(`/transactions/${_id}`, {
      headers: { authorization: getToken() },
    });

    const { data } = res || {};

    dispatch(transactionFetched(data));

    return dispatch(actionSucceeded());
  } catch (err) {
    console.log('error :', err);

    return dispatch(actionFailed(err));
  }
};

export const updateTransaction =
  (transaction) => async (dispatch) => {
    dispatch(actionLoading());

    try {
      const { _id: id } = transaction;

      const res = await axios.put(
        `/transactions/${id}`,
        transaction,
        {
          headers: { authorization: getToken() },
        },
      );

      const { data } = res || {};

      dispatch(transactionUpdated(data));

      return dispatch(actionSucceeded());
    } catch (err) {
      return dispatch(actionFailed(err));
    }
  };

export const linkTransactions =
  (future, settled) => async (dispatch) => {
    dispatch(actionLoading());

    try {
      const { _id: futureId } = future;
      const { _id: settledId } = settled;

      const res = await axios.put(
        `/transactions/${settledId}`,
        future,
        {
          headers: { authorization: getToken() },
        },
      );

      const { data } = res || {};

      dispatch(transactionUpdated(data));
      dispatch(transactionDeleted({ _id: futureId }));

      return dispatch(actionSucceeded());
    } catch (err) {
      console.log('error :', err);

      return dispatch(actionFailed(err));
    }
  };

export const deleteTransaction = (id) => async (dispatch) => {
  dispatch(actionLoading());

  try {
    await axios.delete(`/transactions/${id}`, {
      headers: { authorization: getToken() },
    });

    dispatch(transactionDeleted({ _id: id }));

    return dispatch(actionSucceeded());
  } catch (err) {
    dispatch(actionFailed(err));
  }
};

// selectors
export const selectTransactions = (state) => state.transactions;
export const selectFutureTransactions = (state) =>
  state.transactions.filter(
    (transaction) => transaction.pending && transaction._recurring,
  );
export const selectSettledTransactions = (state) =>
  state.transactions.filter(
    (transaction) => !transaction.pending && transaction._recurring,
  );

export default transactionsSlice.reducer;
