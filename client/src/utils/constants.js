export const stateStatus = {
  IDLE: 'Idle',
  LOADING: 'Loading',
  SUCCEEDED: 'Succeeded',
  FAILED: 'Failed',
};
